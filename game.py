def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()


def game_over(board, space1, space2, space3):
    print_board(board)
    print("GAME OVER")
    print(board[space1], "has won")
    exit()

def row_is_winner(board, row):
    return True

def left_column_is_winner(board, ):
    return True

def middle_column_is_winner(board, ):
    return True

def right_column_is_winner(board, ):
    return True

def left_diagonal_is_winner(board, ):
    return True

def right_diagonal_is_winner(board, ):
    return True                           


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

row1 = board[0:3]
row2 = board[3:6]
row3 = board[6:9]
         

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if board[0] == board[1] and board[1] == board[2]:
        game_over(board, 0, 1, 2)
    elif board[3] == board[4] and board[4] == board[5]:
        game_over(board, 3, 4, 5)
    elif board[6] == board[7] and board[7] == board[8]:
        game_over(board, 6, 7, 8)
    elif board[0] == board[3] and board[3] == board[6]:
        game_over(board, 0, 3, 6)
    elif board[1] == board[4] and board[4] == board[7]:
        game_over(board, 1, 4, 7)
    elif board[2] == board[5] and board[5] == board[8]:
        game_over(board, 2, 5, 8)
    elif board[0] == board[4] and board[4] == board[8]:
        game_over(board, 0, 4, 8)
    elif board[2] == board[4] and board[4] == board[6]:
        game_over(board, 2, 4, 6)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
